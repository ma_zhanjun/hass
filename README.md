 **Home Assistant Configuration** 

原文章连接：

https://github.com/lukevink/hass-config-lajv

https://github.com/matt8707/hass-config

在 Qnap NAS TBS-453A 上建立容器并运行 Home Assistant，自动化在 node-red 上进行，两者结合。

配置文件：https://gitee.com/mx10085/hass

瀚思彼岸：https://bbs.hassbian.com/thread-18153-1-1.html

演示视频地址：

https://www.bilibili.com/video/BV1sG4y1n7K5?share_source=copy_web&vd_source=95e0fc0262decbf71537709eedddafe2


1. 天气信息
![输入图片说明](www/screenshot/weather_01.png)
![输入图片说明](www/screenshot/weather_02.png)
![输入图片说明](www/screenshot/weather_03.png)

2. 日历
![输入图片说明](www/screenshot/calendar_01.png)
![输入图片说明](www/screenshot/calendar_02.png)
![输入图片说明](www/screenshot/calendar_03.png)

3. 播放器
![输入图片说明](www/screenshot/media_01.png)
![输入图片说明](www/screenshot/media_02.png)

4. 灯光设备
![输入图片说明](www/screenshot/all_01.png)
![输入图片说明](www/screenshot/all_02.png)
![输入图片说明](www/screenshot/all_03.png)
![输入图片说明](www/screenshot/all_04.png)
![输入图片说明](www/screenshot/all_05.png)
![输入图片说明](www/screenshot/all_06.png)
![输入图片说明](www/screenshot/all_07.png)
![输入图片说明](www/screenshot/all_08.png)
![输入图片说明](www/screenshot/all_09.png)
![输入图片说明](www/screenshot/all_10.png)
![输入图片说明](www/screenshot/all_11.png)
![输入图片说明](www/screenshot/all_12.png)
![输入图片说明](www/screenshot/all_15.png)

5. 平面图
![输入图片说明](www/screenshot/floorplan_01.png)
![输入图片说明](www/screenshot/floorplan_02.png)
![输入图片说明](www/screenshot/floorplan_03.png)
![输入图片说明](www/screenshot/floorplan_04.png)

6. 其他
![输入图片说明](www/screenshot/sidebar_01.png)
![输入图片说明](www/screenshot/sidebar_02.png)
![输入图片说明](www/screenshot/sidebar_03.png)
![输入图片说明](www/screenshot/sidebar_04.png)
![输入图片说明](www/screenshot/sidebar_05.png)